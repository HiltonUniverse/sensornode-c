#include <Arduino.h>
#include "NodeToRaspberry.h"
#include "JsonFormatter.h"
#include "Sensor.h"
#include "Database.h"

#include <vector>
#define TERMINATE "TERMINATE"
#define DEEPSLEEP_DELAY 6e+7 //3e+6 3seconds //9e+8 15 min 

/*PROTOTYPE FUNCTIONS*/
void fillDataToJasonFormatterVector();
void packetController();
void detectDeviceI2C();

std::vector<String> dataToJsonFormatter;
String jsonString = "";

const char* ssid = "mpv1-gateway"; //YOUR WIFI NETWORK NAME
const char* password = "e2bb56826b8ce1d7a30a5fd3984691f0"; //YOUR WIFI PASSWARD

/**
 * setup()
 * 
 * Configure/connect to the wifi hotspot of the gateway and initialize the Database File.
 */
void setup() {
  Serial.begin(9600);
  sensorNode.configureWIFI(ssid, password);
  db.initAFile();
}

/**
 * loop()
 * 
 * First, store data from each sensor to a container.
 * Second, convert the container's value to JSON formatt.
 * Third, send the packet to the gateway if possible.
 * Finally, go to deep sleep mode for specified DEEPSLEEP_DELAY time.
 */
void loop() {
  //data to be formatted to JSON
  fillDataToJasonFormatterVector();
  //JSON formatted data
  jsonString = jsFormatter.getJsonObject(dataToJsonFormatter);
  //send the data
  packetController();
  ESP.deepSleep(DEEPSLEEP_DELAY, WAKE_RF_DEFAULT); // 3 seconds
}

/**
 * packetController()
 * 
 * Controlls the flow of packet.
 * If the node is connect to the gateway, we determine wheather there is data 
 * Or not in the Database of node. If there is no data, we send the current
 * packet to the gateway. If there is data, then we send all the data from the Database
 * alone with the new packet.
 * 
 * If the node is not connected to the gateway, then we start storing the data in the local Database of the node.
 * This stored data in the node is then sent to the gateway after it comes online.
 * 
 */
void packetController() {
  int clientStatus = sensorNode.connectToPi();// connect to the raspberry pi hotspot
  Serial.println(clientStatus);
  if (clientStatus == 1) { // if true, client is found and data transfer is possible
    if (!db.isDataInDB()) { //if nothing in the database send current packet
      sensorNode.sendNewPacket(jsonString);
      jsonString = "";
      sensorNode.sendNewPacket(TERMINATE);
    } else {  //if data in db, send data from db.

      File database = db.getDB();
      while (database.available()) {
        String dbPacket = database.readStringUntil('\n');
        Serial.println(dbPacket);
        sensorNode.sendNewPacket(dbPacket);
      }
      database.close();
      sensorNode.sendNewPacket(jsonString);
      sensorNode.sendNewPacket(TERMINATE);
      db.reInitDB();
    }
  } else {    //write data to the local db if gateway is off.
    Serial.print("Gateway Out of Service.");
    Serial.println(" Writing into the Local database.");
    //write in to the file
    db.writeIntoDB(jsonString);
  }
}

/**
 * fillDataToJasonFormatterVector()
 * 
 * store data from each sensor to a container. This data is used to make a JSON formatted packet.
 * 
 */
void fillDataToJasonFormatterVector() {
  dataToJsonFormatter.clear();
  dataToJsonFormatter.push_back(sensorNode.getMacAddressOfSensorNode());
  dataToJsonFormatter.push_back((String) sensor.getTemperature_DHT());
  dataToJsonFormatter.push_back((String) sensor.getHumidity_DHT());
  auto v = sensor.getSpectrum();
  for (auto & val : v) {
    dataToJsonFormatter.push_back((String) val);
  }
  dataToJsonFormatter.push_back((String) sensor.getLux());
}



