#include "NodeToRaspberry.h"

/**
 * getMacAddressOfSensorNode() 
 * 
 * Returns:
 *  The mac address of the ESP8266.
 */
String NodeToRaspberry::getMacAddressOfSensorNode() {
  return WiFi.macAddress();
}
 
/**
 * configureWIFI()
 * 
 * Connects the ESP8266 to wifi with matching username(AKA ssid) and password.
 * 
 * Returns:
 *  wifiStatusValue: 1 = if connection success.
 *                  -1 = if connection failed.
 */
int NodeToRaspberry::configureWIFI(const char* ssid, const char* password) {

  int wifiStatusValue = -1;
  m_ssid = ssid;
  m_password = password;
  
  //number of tries to connect to the wifi of gateway
  int wifiConAttemptCounter = 0;
  
  WiFi.persistent(false);
  WiFi.mode(WIFI_OFF);
  WiFi.mode(WIFI_STA);
  
  // Connect to WiFi
  WiFi.begin(ssid, password);
  // while wifi not connected yet, print '.'
  // then after it connected, get out of the loop
  while (WiFi.status() != WL_CONNECTED && wifiConAttemptCounter < 15) {
    delay(500);
    wifiConAttemptCounter++;
    Serial.print(".");
    if (WiFi.status() == WL_NO_SSID_AVAIL) { //if no wifi found
      break;
    }
  }

  if (WiFi.status() == WL_CONNECTED) { //if wifi connected
    wifiStatusValue = 1;
    //print a new line, then print WiFi connected and the IP address
    Serial.println("");
    Serial.println("WiFi connected");
  } else {  // if wifi not connected
    wifiStatusValue = -1;
    //Serial.println("No SSID Found!");
    Serial.println();
    Serial.println("Couldn't connect to the Gateway!");
  }
  return wifiStatusValue;
}

/**
 * connectToPi()
 * 
 * Checks if the client is connected to the gateway. If not connected, tries to connect to it.
 * 
 * Returns:
 *  clientStatus: 
 *      1 = if client is connected.
 *  
 *  else:
 *      0 = if couldn't connect to the gateway.
 *      
 */
int NodeToRaspberry::connectToPi() {
  if (clientStatus == 1) return clientStatus;
  if (WiFi.status() != WL_NO_SSID_AVAIL) {
    if (WiFi.status() != WL_CONNECTED) {
      configureWIFI(m_ssid, m_password); //try to connect to the wifi
    }
    if (WiFi.status() == WL_CONNECTED && WiFi.status() != WL_NO_SSID_AVAIL) { //check if we are really connected, if yes connect the client
      clientStatus = client.connect({192, 168, 3, 141}, 8080);
    }
  }
  return clientStatus;
}


/**
 * sendNewPacket()
 * 
 * Sends JSON formatted data packet to the gateway.
 * After complete transaction, it receives and ACK. packet as a confirmation.
 * 
 * Parameters:
 *  @ String data - JSON formatted data to be sent to the gateway.
 */
void NodeToRaspberry::sendNewPacket(String data) {
  //send packet to the gateway.
  client.print(data);
  //size of ACK packets received from the gateway.
  int numberOfBytes = -1;
  while (1) {
    //Receive ACK
    if (client.available() > 0) {
      numberOfBytes = client.available();
      break;
    }
  }

  //Print the ACK message sent from the gateway.
  for (size_t b = 0; b < numberOfBytes; b++)
  {
    Serial.print((char) client.read());
  }
  Serial.println("");
}

NodeToRaspberry sensorNode = NodeToRaspberry();

