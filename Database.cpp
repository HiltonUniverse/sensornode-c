#include "Database.h"

/**
 * Database()
 * 
 * Initialize the FileSystem in the ESP8266.
 */
Database::Database() {
  delay(10);
  SPIFFS.begin();
}

/**
 * initAFile()
 * 
 * Creates a file in the Local Database called "data_db.txt" if it was not created before.
 */
void Database::initAFile() {
  database = SPIFFS.open("/data_db.txt", "a+"); //a file is created if it doesn't exist
  if (!database) {
    Serial.println("File open was Unsuccessfull!");
  }
}


/**
 * writeIntoDB()
 * 
 * Stores data in the database.
 * 
 * Parameters:
 * @ String jsonString - data to be store in the database.
 */
void Database::writeIntoDB(String jsonString) {
  database = SPIFFS.open("/data_db.txt", "a");
  database.println(jsonString);
  Serial.println(jsonString);
  jsonString = "";
  database.close();
}

/**
 * Not Being Used due to complications with insufficient heap memory when storing 
 * large number of data in a vector! 
 */
std::vector<String> Database::readFromDB() {
  std::vector<String> dataFromDB;
  dataFromDB.clear();

  database = SPIFFS.open("/data_db.txt", "r");
  if (!database) {
    Serial.println("data_db.txt file open failed on read.");
  } else {
    Serial.println("Packets from Database: ");
    while (database.available()) {
      String value = database.readStringUntil('\n');
      //Serial.print("Heap space:");
      //Serial.print(ESP.getFreeHeap());
      dataFromDB.push_back(value);
    }
  }
  database.close();

  return dataFromDB;
}

/**
 * isDataInDB()
 * 
 * Is there is any data in the local database?.
 * 
 * Returns:
 * 
 * true if:
 *  There are any data available in the database.
 */
bool Database::isDataInDB() {
  if (database.available()) {
    isThereDataInDB = true;
  } else {
    isThereDataInDB = false;
  }
  return isThereDataInDB;
}

/**
 * reInitDB()
 * 
 * Removes previous file from the file system and re-creates it.
 */
void Database::reInitDB() {
  SPIFFS.remove("/data_db.txt");
  initAFile();
}

/**
 * getDB()
 * 
 * Returns:
 *  Database Object.
 */
File Database::getDB() {
  return database;
}

Database db = Database();
