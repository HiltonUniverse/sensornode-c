#ifndef DB_H
#define DB_H

#include <Arduino.h>
#include <FS.h>
#include <vector>

class Database {
  public:
    Database();
    void initAFile();
    void writeIntoDB(String value);
    std::vector<String> readFromDB();
    bool isDataInDB();
    void reInitDB();
    File getDB();
  private:
    bool isThereDataInDB = false;
    File database;


};

extern Database db;
#endif
