#include "Sensor.h"

void lightInt();
bool lightIntHappened = false;

DHT dht(DHTPIN, DHTTYPE);
APDS9301 apds;
AS726X as726X;

/**
 * Sensor()
 * 
 * Initialize all the sensors
 */
Sensor::Sensor() {
  delay(5);
  Serial.begin(9600);
  Wire.begin();

  apds.begin(0x39);
  apds.setGain(APDS9301::LOW_GAIN);
  apds.setIntegrationTime(APDS9301::INT_TIME_13_7_MS);
  apds.setLowThreshold(0);
  apds.setHighThreshold(50);
  apds.setCyclesForInterrupt(1);
  apds.enableInterrupt(APDS9301::INT_ON);
  apds.clearIntFlag();

  pinMode(INT_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(16), lightInt, FALLING);
  Serial.println(apds.getLowThreshold());
  Serial.println(apds.getHighThreshold());

  /*Check if any sensor from the I2C is removed*/
  bool foundSensor = false;
  detectDeviceI2C();
  for (auto& a : addr) {
    if (a == 73)
      foundSensor = true;
  }

  if (foundSensor) {
    i2cErrorFound = false;
    as726X.begin();
  } else {
    i2cErrorFound = true;
  }

  dht.begin();
}

/**
 * getTemperature_DHT()
 * 
 * Gets the temperature reading form the DHT sensor.
 * 
 * Returns:
 *  Temperature sensor value.
 */
float Sensor::getTemperature_DHT() {
  int sensorValue = dht.readTemperature(false, false);
  int count = 0;
  while ((isnan(sensorValue) || sensorValue > 100) && count < 10) {
    sensorValue = dht.readTemperature(false, false);
    count = count + 1;
    delay(50);
  }

  if (sensorValue > 100)
    return NAN;

  return sensorValue;
}

/**
 * getHumidity_DHT()
 * 
 * Gets the humidity reading from the DHT sensor.
 * 
 * Returns:
 *  Humidity sensor value.
 */
float Sensor::getHumidity_DHT() {
  int sensorValue = dht.readHumidity();
  int count = 0;
  while ((isnan(sensorValue) || sensorValue > 100) && count < 10) {
    sensorValue = dht.readHumidity();
    count = count + 1;
    delay(50);
  }

  if (sensorValue > 100)
    return NAN;

  return sensorValue;
}

/**
 * getLux()
 * 
 * Gets the Lux from the light sensor.
 * 
 * Returns:
 *  Lux value.
 */
float Sensor::getLux() {
  float returnValue = apds.readLuxLevel();
  int count = 0;
  while (isinf(returnValue) && count < 10) {
    returnValue = apds.readLuxLevel();
    count = count + 1;
    delay(50);
  }

  if (isinf(returnValue))
    returnValue = NAN;

  return returnValue;
}

/**
 * Not used because we already have a descrete temperature sensor!
 */
float Sensor::getTemperature_as726X() {
  as726X.takeMeasurements();
  return as726X.getTemperature();
}

/**
 * getSpectrum()
 * 
 * Gets all the possible sprectums of the light.
 * 
 * Returns:
 *  If sensor is connected:
 *      measurements from the Light spectrum sensor.
 *   
 *  else: 
 *      NAN - not a number.
 */
std::vector<float>& Sensor::getSpectrum() {
  spectrum.clear();
  if (!i2cErrorFound) {
    delay(10);
    as726X.takeMeasurements();
    spectrum.push_back(as726X.getCalibratedViolet());
    spectrum.push_back(as726X.getCalibratedBlue());
    spectrum.push_back(as726X.getCalibratedGreen());
    spectrum.push_back(as726X.getCalibratedYellow());
    spectrum.push_back(as726X.getCalibratedOrange());
    spectrum.push_back(as726X.getCalibratedRed());
  } else {
    spectrum.push_back(NAN);
    spectrum.push_back(NAN);
    spectrum.push_back(NAN);
    spectrum.push_back(NAN);
    spectrum.push_back(NAN);
    spectrum.push_back(NAN);
  }

  return spectrum;
}

/**
 * lightInt()
 * 
 * Call back function for the initializatio of the sensors.
 */
void lightInt()
{
  lightIntHappened = true;
}

/**
 * detectDeviceI2C()
 * 
 * Detects all the slaves in a I2C network.
 */
void Sensor::detectDeviceI2C()
{
  byte error, address;
  int nDevices;
  int count = 0;

  while (count < 1) {
    for (address = 1; address < 127; address++ )
    {
      Wire.beginTransmission(address);
      error = Wire.endTransmission();

      if (error == 0)
      {
        addr.push_back((int) address);
        nDevices++;
      }
    }
    delay(2000);
    count ++;
  }
}


Sensor sensor = Sensor();
