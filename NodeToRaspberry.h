#ifndef NODETORASPBERRY_H
#define NODETORASPBERRY_H

#include <Arduino.h>
#include <ESP8266WiFi.h>

class NodeToRaspberry {
  public:
    NodeToRaspberry() = default;
    String getMacAddressOfSensorNode();
    int configureWIFI(const char* ssid, const char* password);
    int connectToPi();
    void sendNewPacket(String data);

  private:
    WiFiClient client;
    bool isSSIDFound = false;
    int clientStatus = 0;
    const char* m_ssid; //YOUR WIFI NETWORK NAME
    const char* m_password; //YOUR WIFI PASSWARD

};

extern NodeToRaspberry sensorNode;

#endif NODETORASPBERRY_H
