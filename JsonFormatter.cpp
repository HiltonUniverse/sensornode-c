#include "Arduino.h"
#include "JsonFormatter.h"

/*Buffer to store the JSON formatted data*/
StaticJsonBuffer<JSON_SIZE> jsonBuffer;

/**
 * getJsonObject()
 * 
 * Creates a JSON formatted data packet from given data.
 * 
 * Parameters:
 * @ std::vector<String>& data - Container, that contains data from sensors.              
 * 
 * Returns:
 *  The JSON formatted data as a string.
 */
String JsonFormatter::getJsonObject(std::vector<String>& data) {
  jsonBuffer.clear();
  jsonString = "";
  JsonObject& root = jsonBuffer.createObject(); //create a json root object
  root["node-id"] = data.at(0); //create an json object inside the root
  JsonArray& senData = root.createNestedArray("sensorData");  //create a json array inside the root object

  JsonObject& data1 = senData.createNestedObject(); //create json arrays for sensor data inside the senData json array.
  data1["sensor_type"] = "temperature-modelA";
  data1["value"] = data.at(1);

  JsonObject& data2 = senData.createNestedObject(); //create json arrays for sensor data inside the senData json array.
  data2["sensor_type"] = "humidity-modelA";
  data2["value"] = data.at(2);

  JsonObject& data3 = senData.createNestedObject(); //create json arrays for sensor data inside the senData json array.
  data3["sensor_type"] = "irradiance-model16";
  JsonObject& dta = data3.createNestedObject("value");
  dta["violet"] = data.at(3);
  dta["blue"] =  data.at(4);
  dta["green"] = data.at(5);
  dta["yellow"] = data.at(6);
  dta["orange"] = data.at(7);
  dta["red"] = data.at(8);

  JsonObject& data4 = senData.createNestedObject(); //create json arrays for sensor data inside the senData json array.
  data4["sensor_type"] = "irradiance-total";
  data4["value"] = data.at(9);

  root.printTo(jsonString); //prettyPrintTo(jsonString);//convert this JSON formatted object to String.
  jsonString += "\0";

  
  return jsonString;//return the converted String.
}

JsonFormatter jsFormatter = JsonFormatter();

