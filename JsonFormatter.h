#ifndef JSON_H
#define JSON_H

#include <Arduino.h>
#include <ArduinoJson.h>

const int JSON_SIZE = 5000;

class JsonFormatter{
  public:
    JsonFormatter() = default;
    String getJsonObject(std::vector<String>& data);
  private:
    String jsonString;
};

extern JsonFormatter jsFormatter;

#endif
